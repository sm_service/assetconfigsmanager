﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace mce.ConfigsLoader.Editor
{
    public static class ConfigsLoaderUtils
    {
        public static IReadOnlyList<string> ToLines(this string text)
        {
            text = text.Replace("\r\n", "\n").Replace("\"\"", "[_quote_]");

            var matches = Regex.Matches(text, "\"[\\s\\S]+?\"");

            foreach (Match match in matches)
            {
                text = text.Replace(match.Value,
                    match.Value.Replace("\"", null).Replace(",", "[_comma_]").Replace("\n", "[_newline_]"));
            }

            // Making uGUI line breaks to work in asian texts.
            text = text.Replace("。", "。 ").Replace("、", "、 ").Replace("：", "： ").Replace("！", "！ ").Replace("（", " （").
                Replace("）", "） ").Trim();

            return text.Split('\n').Where(i => i != "").ToList();
        }

        public static IReadOnlyList<string> ToColumns(this string line)
        {
            return line.Split(',').Select(j => j.Trim()).Select(j =>
                j.Replace("[_quote_]", "\"").Replace("[_comma_]", ",").Replace("[_newline_]", "\n")).ToList();
        }
    }
}