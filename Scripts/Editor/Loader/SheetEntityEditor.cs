﻿using UnityEditor;
using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CustomPropertyDrawer(typeof(SheetEntity))]
    public class SheetEntityEditor : PropertyDrawer
    {
        private const float _sizeElement = 20;
        private const float _deltaXElement = 4;
        private const float _deltaYElement = 5;
        private const float _deltaDownElement = 70;
        private Rect _topRect;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var assets = property.FindPropertyRelative("AssetsList");
            float totalHeight = EditorGUI.GetPropertyHeight(assets, label);
            return _sizeElement * 2 + totalHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;


            _topRect = new Rect(position.x, position.y, position.width, _sizeElement);
            RepaintTopPanel(property);

            var listLabelRect = new Rect(position.x, position.y + _sizeElement + _deltaYElement,
                position.width + _deltaDownElement, _sizeElement);
            EditorGUI.LabelField(listLabelRect, "Assets:");


            var listRect = new Rect(position.x + _deltaDownElement, position.y + _sizeElement + _deltaYElement,
                position.width - _deltaDownElement, position.height - _sizeElement - _deltaYElement);
            EditorGUI.PropertyField(listRect, property.FindPropertyRelative("AssetsList"), GUIContent.none);


            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        private void RepaintTopPanel(SerializedProperty property)
        {
            var url = property.FindPropertyRelative("GoogleUrl");
            var name = property.FindPropertyRelative("NameSheet");
            EditorGUI.LabelField(GetTopRect(0, 0.05f), "Table:");
            EditorGUI.PropertyField(GetTopRect(0.05f, 0.20f), name, GUIContent.none);
            EditorGUI.LabelField(GetTopRect(0.20f, 0.25f), "Url:");
            EditorGUI.PropertyField(GetTopRect(0.25f, 0.7f), url, GUIContent.none);
            if (GUI.Button(GetTopRect(0.7f, 0.85f), "Reload"))
            {
                var manager = property.serializedObject.targetObject as ConfigsLoader;
                manager.ReloadTable(url.stringValue);
            }

            if (GUI.Button(GetTopRect(0.85f, 1f), "Open"))
            {
                var manager = property.serializedObject.targetObject as ConfigsLoader;
                manager.OpenUrl(url.stringValue);
            }
        }

        private Rect GetTopRect(float xStart, float xEnd)
        {
            return new Rect(_topRect.x + (_topRect.width * xStart), _topRect.y,
                _topRect.width * (xEnd - xStart) - _deltaXElement, _topRect.height);
        }
    }
}