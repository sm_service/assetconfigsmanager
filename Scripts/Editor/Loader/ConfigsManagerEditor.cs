﻿using UnityEditor;
using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CustomEditor(typeof(ConfigsLoader))]
    public class ConfigsManagerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var manager = (ConfigsLoader) target;
            
            DrawDefaultInspector();
            var buttonStyle = new GUIStyle(GUI.skin.button) { fontStyle = FontStyle.Bold, fixedHeight = 30 };

            if (GUILayout.Button("FindAllConverters", buttonStyle))
            {
                manager.FindAllConverters();
            }
            
            if (GUILayout.Button("ReloadAllTable", buttonStyle))
            {
                manager.ReloadAllTable();
            }
            
        }
    }
}