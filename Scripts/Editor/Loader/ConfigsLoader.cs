﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CreateAssetMenu(menuName = "Configs/ConfigsLoader", fileName = "ConfigsLoader")]
    public class ConfigsLoader : ScriptableObject
    {
        [SerializeField] private List<ConfigConverter> _convertersList = new List<ConfigConverter>();

        [SerializeField] private List<SheetEntity> _googleSheets = new List<SheetEntity>();


        private const string DownloadUrlPattern = "https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&sheet={1}";
        private const string OpenUrlPattern = "https://docs.google.com/spreadsheets/d/{0}";

        public void OpenUrl(string urlValue)
        {
            var sheet = _googleSheets.FirstOrDefault(x => x.GoogleUrl == urlValue);
            var url = string.Format(OpenUrlPattern, sheet.GoogleUrl);
            System.Diagnostics.Process.Start(url);
        }

        public void ReloadTable(string urlValue)
        {
            var sheet = _googleSheets.FirstOrDefault(x => x.GoogleUrl == urlValue);
            if (sheet == null)
            {
                Debug.LogError($"[Configs] not Found table Url : {urlValue}");
                return;
            }

            foreach (var asset in sheet.AssetsList)
            {
                var url = string.Format(DownloadUrlPattern, sheet.GoogleUrl, asset.name);
                var downloadText = DownloadGoogleSheet(url);
                ParseCsvToAsset(downloadText, asset);
                Debug.Log($"[Configs] Пересобран конфиг {asset.name}");
            }
            Debug.Log($"[Configs] Пересобраны все конфиги в таблице {sheet.NameSheet}");
        }

        public void ReloadAllTable()
        {
            foreach (var sheet in _googleSheets)
            {
                ReloadTable(sheet.GoogleUrl);
            }
        }

        public void FindAllConverters()
        {
            _convertersList.Clear();
            var guids = AssetDatabase.FindAssets($"t:{typeof(ConfigConverter).Name}");

            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var converter = AssetDatabase.LoadAssetAtPath<ConfigConverter>(path);

                if (converter != null)
                    _convertersList.Add(converter);
            }

            SaveAsset(this);
            Debug.Log("[Configs] End FindAllConverters");
        }


        private void SaveAsset(ScriptableObject asset)
        {
            EditorUtility.SetDirty(asset);
            AssetDatabase.SaveAssets();
        }


        private string DownloadGoogleSheet(string url)
        {
            var client = new GoogleWebClient(new CookieContainer());
            var result = client.DownloadString(url);
            if (string.IsNullOrEmpty(result))
            {
                Debug.LogError("Пришел пустой отве");
            }
            if (result.Contains("signin/identifier"))
            {
                Debug.LogError("[Configs] Нет доступа");
            }
            
            return result;
        }
        
        
        
        private void ParseCsvToAsset(string sheetText, ConfigsAsset asset)
        {
            var type = asset.GetType();
            var baseType = type.BaseType;
            
            var geneticsConfig = baseType.GetGenericArguments();
            var configType = geneticsConfig[0];
            
            var methodClear = baseType.GetMethod("Clear", BindingFlags.Public | BindingFlags.Instance);
            methodClear.Invoke(asset, null);
            
            var methodAddConfig = baseType.GetMethod("AddConfig", BindingFlags.Public | BindingFlags.Instance);

            var lines = sheetText.ToLines();
			var nameFields = lines[0].Split(',').Select(i => i.Trim()).ToList();
            

            var fields = configType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            var filedToColumIndex = new List<(FieldInfo, int)>();
            for (int i = 0; i < fields.Length; i++)
            {
                for (int j = 0; j < nameFields.Count; j++)
                {
                    if (fields[i].Name == nameFields[j])
                    {
                        filedToColumIndex.Add((fields[i], j));
                        continue;
                    }
                }
            }

            for (var i = 1; i < lines.Count; i++)
			{
                var config = Activator.CreateInstance(configType);
                
				var fieldValues = lines[i].ToColumns();

                foreach (var para in filedToColumIndex)
                {
                    var valueType = para.Item1.FieldType;
                    var value = fieldValues[para.Item2];
                    var parseValue = ConvertAbstractValue(valueType,value, true);
                    para.Item1.SetValue(config, parseValue);
                }

                object[] parameters = new[] { config };
                methodAddConfig.Invoke(asset, parameters);
            }
            
            
            SaveAsset(asset);
		}
        private object ConvertAbstractValue(Type valueType, string value, bool isList)
        {
            var managerType = GetType();
            var nameMethod = nameof(ConvertValue);
            var methodGetValue = managerType.GetMethod(nameMethod, BindingFlags.NonPublic | BindingFlags.Instance);

            var generic = methodGetValue.MakeGenericMethod(valueType);
            var result = generic.Invoke(this, new [] { value , isList.ToString()});
            return result;
        }
        private object ConvertValue<TValue>(string value, string findList)
        {
            if (string.IsNullOrEmpty(value) || value == "\"")
                return null;
            
            var type = typeof(TValue);

            if (type == typeof(Int32))
            {
                if (Int32.TryParse(value, out var intValue))
                    return (TValue)Convert.ChangeType(intValue, type);
                return 0;
                
            }

            if (type == typeof(float))
            {
                if (float.TryParse(value, out var floatValue))
                    return (TValue)Convert.ChangeType(floatValue, type);
                return 0;
            }

            if (type.IsEnum)
            {
                var res = Enum.Parse(type, value);
                return (TValue)Convert.ChangeType(res, type) ;
            }
            
            if (type == typeof(bool))
            {
                if (value == "true" || value == "TRUE")
                {
                    return true;
                }

                return false;
            }
            

            if (type == typeof(string))
            {
                return (TValue)Convert.ChangeType(value, typeof(TValue)) ;
            }
            
            if (string.IsNullOrEmpty(value))
            {
                return (TValue)Convert.ChangeType(null, type) ;
            }

            
            if (findList == true.ToString())
            {
                
                var listValues = value.Split(';');
                if (listValues.Length > 1)
                {
                    
                    var geneticsItems = type.GetGenericArguments();
                    var configType = geneticsItems[0];
                    var listType = typeof(List<>).MakeGenericType(configType);
                    
                    var res = Activator.CreateInstance(listType) as IList;
                    foreach (var str in listValues)
                    {
                        if(string.IsNullOrEmpty(str))
                            continue;
                        
                        var listValue = ConvertAbstractValue(configType, str, false);
                        var vv = Convert.ChangeType(listValue, configType) ;
                        res.Add(vv);
                    }

                    return res;
                }
            }
            

            foreach (var converter in _convertersList)
            {
                if (!converter.IsGoodConverter<TValue>())
                    continue;

                var res = converter.GetObject(value);
                
                if (res != null)
                    return (TValue)Convert.ChangeType(res, type) ;
            }

            Debug.LogError($"Не получилось конвертировать строку :{value}  в : {type}");
            return null;
        }
    }
}