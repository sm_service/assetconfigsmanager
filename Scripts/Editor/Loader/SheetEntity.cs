﻿using System;
using System.Collections.Generic;

namespace mce.ConfigsLoader.Editor
{
    [Serializable]
    public class SheetEntity
    {
        public string NameSheet;
        public string GoogleUrl;
        public List<ConfigsAsset> AssetsList;
    }
}