﻿using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CreateAssetMenu(menuName = "Configs/ConverterDefault/SpriteConverter", fileName = "SpriteConverter", order = 0)]
    public class SpriteConverter : ConfigObjectConverter<Sprite>
    {

    }
}