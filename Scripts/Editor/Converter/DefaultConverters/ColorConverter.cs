﻿using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CreateAssetMenu(menuName = "Configs/ConverterDefault/ColorConverter", fileName = "ColorConverter", order = 0)]
    public class ColorConverter: ConfigBaseConverter<Color>
    {
        public override object GetObject(string nameObject)
        {
            if(ColorUtility.TryParseHtmlString(nameObject, out var color))
                return color;

            Debug.LogError($" Cant covert  {nameObject} to color");
            return new Color(1, 1, 1, 1);
        }
    }
}