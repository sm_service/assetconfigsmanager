﻿using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CreateAssetMenu(menuName = "Configs/ConverterDefault/IdFloatConverter", fileName = "IdFloatConverter", order = 0)]
    public class IdFloatConverter : ConfigBaseConverter<IdFloatPara>
    {
        public override object GetObject(string nameObject)
        {
            if (string.IsNullOrEmpty(nameObject))
                return null;
            return new IdFloatPara(nameObject);
        }
    }
}