﻿using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    [CreateAssetMenu(menuName = "Configs/ConverterDefault/IdIntConverter", fileName = "IdIntConverter", order = 0)]
    public class IdIntConverter : ConfigBaseConverter<IdIntPara>
    {
        public override object GetObject(string nameObject)
        {
            return new IdIntPara(nameObject);
        }
    }
}