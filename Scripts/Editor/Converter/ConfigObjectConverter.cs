﻿using System.Collections.Generic;
using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    //[CreateAssetMenu(menuName = "Configs/Converter/NameTObject", fileName = "NameTObject", order = 0)]
    public abstract class ConfigObjectConverter<TObject>: ConfigConverter where TObject : Object
    {
        [SerializeField]
        private List<TObject> assets;
        
        public override bool IsGoodConverter<T>()
        {
            return typeof(TObject) == typeof(T);
        }

        public override object GetObject(string nameObject) 
        {
            foreach (var asset in assets)
            {
                if(asset == null)
                    continue;
                if (asset.name == nameObject)
                    return asset;
            }

            return null;
        }
        
    }
}