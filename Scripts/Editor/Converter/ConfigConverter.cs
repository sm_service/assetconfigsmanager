﻿using UnityEngine;

namespace mce.ConfigsLoader.Editor
{
    public abstract class ConfigConverter: ScriptableObject
    {
        public Object Object => this;

        public string Name => name;

        public virtual bool IsGoodConverter<T>()
        {
            return false;
        }
        
        public virtual object GetObject(string nameObject)
        {
            return null;
        }
        
    }
}