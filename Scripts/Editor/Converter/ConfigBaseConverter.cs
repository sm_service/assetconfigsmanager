﻿
namespace mce.ConfigsLoader.Editor
{
    //[CreateAssetMenu(menuName = "Configs/Converter/NameTObject", fileName = "NameTObject", order = 0)]
    public abstract class ConfigBaseConverter<TObject>: ConfigConverter
    {
        public override bool IsGoodConverter<T>()
        {
            return typeof(TObject) == typeof(T);
        }

        public override object GetObject(string nameObject) 
        {
            return null;
        }
        
    }
}