﻿using System;
using UnityEngine;

namespace mce.ConfigsLoader
{
    [Serializable]
    public class IdFloatPara
    {
        [SerializeField]
        private int id;

        [SerializeField]
        private float value;
        
        public int ID => id;
        public float Value => value;
        
        public IdFloatPara(string str)
        {
            var values = str.Split('_');
            if (Int32.TryParse(values[0], out var idInt))
                id = idInt;
            else
                Debug.LogError($" Cant covert  {values[0]} to int");
            

            if (float.TryParse(values[1], out var valueFloat))
                value = valueFloat;
            else
                Debug.LogError($" Cant covert  {values[1]} to float");
        }
    }
}