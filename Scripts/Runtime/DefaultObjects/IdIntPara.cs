﻿using System;
using UnityEngine;

namespace mce.ConfigsLoader
{
    [Serializable]
    public class IdIntPara
    {
        [SerializeField]
        private int id;

        [SerializeField]
        private int value;
        
        public int ID => id;
        public int Value => value;
        
        public IdIntPara(string str)
        {
            var values = str.Split('_');
            if (Int32.TryParse(values[0], out var idInt))
                id = idInt;
            else
                Debug.LogError($" Cant covert  {values[0]} to int");
            
            if (Int32.TryParse(values[1], out var valueInt))
                value = valueInt;
            else
                Debug.LogError($" Cant covert  {values[1]} to int"); 
        }
    }
}