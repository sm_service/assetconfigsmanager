﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace mce.ConfigsLoader
{
    //[CreateAssetMenu(menuName = "Configs/Assets/PathToSheetList", fileName = "NameListInSheet", order = 0)]
    public abstract class ConfigsAsset<TConfig> : ConfigsAsset where TConfig: Config
    {
        [SerializeField]
        protected List<TConfig> configs = new List<TConfig>();

        public IReadOnlyList<TConfig> Configs => configs;

#if UNITY_EDITOR

        public void Clear()
        {
            configs.Clear();
        }
        
        public void AddConfig(TConfig config)
        {
            configs.Add(config);
        }
                
#endif

    }
    
    public abstract class ConfigsAsset : ScriptableObject
    {
        public virtual bool IsGoodConverter<T>()
        {
            return false;
        }
        
        public virtual object GetObject(string nameObject)
        {
            return null;
        }
    }
}