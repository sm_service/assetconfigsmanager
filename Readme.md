﻿# ConfigsLoader

## Info
### -Open access to the Google spreadsheet.
### -Create ConfigsLoader SO. "Configs/ConfigsLoader" (in editor folder).
### -Create GoogleSheetItem in ConfigsLoader
### -Add Name GoogleSheet and ctr+v GoogleUrl
### -Create ConfigsAssets SO.
### -Move ConfigsAssets SO to GoogleSheetItem.
### -Click Reload

## Step1
![image info](Image/Step1.png)

## Step2
![image info](Image/Step2.png)

## Step3
![image info](Image/Step3.png)